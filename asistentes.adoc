﻿== ﻿Lista de asistentes

// Ordenados por orden alfabético
// Formato: Apellidos, Nombre

* Bazán Romero, José Antonio
* Casanueva Morato, Daniel
* Cortes Vazquez, Jaime
* De los Santos Cuéllar, Arturo
* Galeano Espinosa, Antonio
* Hidalgo Muñoz de Rivera, Alejandro
* Iglesias Dominguez, Javier
* Jimenez Quintero, Eduardo
* Jiménez Romero, Jesús
* Juan Chico, Jorge
* Viera Chaves, Fco. Javier
* Lobato Salas, Jesús
* Moya Aragón, Jaime
* Olabarrieta Eduardo, Iraia
* Peña García, José Antonio
* Posada Lopez, Maria Jose
* Montaño Tenor, Jesús Carlos
* Sánchez Cano, Miguel Ángel
* Sánchez Cuevas, Pablo
* Serrano Fernández, Jesús
* Vázquez Pérez, Antonio


=== Organización del transporte

// Si tienes vehículo, pon el número de plazas. Si no tienes, añádete a
// alguno de los vehículos existentes.

==== Bicicleta de Jesus Carlos (2 plazas)

* Jesus Carlos Montaño Tenor

==== Moto de Jorge (2 plazas)

* Jorge Juan-Chico
* Jimenez Quintero, Eduardo

==== Coche de Javi Viera (5 plazas)

* Javi Viera

==== Citroën DS de antonio (5 plazas)

* Alejandro Hidalgo Muñoz de Rivera
* Antonio Vázquez Pérez

==== Coche de José Antonio (5 plazas)

* José Antonio Peña García

==== Coche de Jaime (4 plazas)

* Jaime Cortes Vazquez
* Javier Iglesias Dominguez 
* Jesús Serrano Fernández

==== Renault 19 de Dani

* Casanueva Morato, Daniel (conductor sobrio)
* Sánchez Cuevas, Pablo (copiloto, el que tiene el Google Maps)
* Posada Lopez, Maria Jose (la que controla la música)


==== Coche de Jesús (4 plazas)

* Jiménez Romero Jesús


==== Coche de Jesús Lobato (5 plazas)

* Bazán Romero, José Antonio
* Lobato Salas, Jesús
* Olabarrieta Eduardo, Iraia

==== Coche de Jaime Moya (4 plazas)

* Moya Aragón, Jaime

==== Coche de Antonio Galeano (4 plazas)

* Antonio Galeano

==== Vespa de Miguel Ángel (1 plaza)

* Sánchez Cano, Miguel Ángel
